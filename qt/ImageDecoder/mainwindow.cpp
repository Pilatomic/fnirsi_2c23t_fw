#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <QFileDialog>
#include <QRgb>

#define BASE_ADDR   0x8008000

void MainWindow::write_byte_framebuffer_or_display(int x_pos,int y_pos,uint16_t color)

{
    //qDebug() << "Draw" << x_pos << y_pos << color;

    ((uint16_t*)m_screenImage.bits())[x_pos + y_pos * 320] = color;
    /*m_screenImage.setPixel(x_pos,y_pos,QColor((color >> 13) & 0x1F,
                                            (color >> 5 ) & 0x3F,
                                              (color & 0x1F), 255).rgb());*/
   /* if (PTR_RAM_POOL_AREA_0800beac->directDrawToDisplay != 0) {
        LCD_writeFbCmd(x_pos,y_pos,x_pos,y_pos);
        *(uint16_t *)PTR_EXT_RAM_0800beb0 = color;
        return;
    }
    PTR_RAM_POOL_AREA_0800beac->framebuffer[y_pos * 320 + x_pos + 3] = color;
    return;*/
}


void MainWindow::compute_draw_coord_for_alignment
    (int alignment,int area_w,int area_h,int area_x,int area_y,uint *draw_x,uint *draw_y)

{
    uint uVar1;
    uint uVar2;

    uVar2 = (160 - area_w / 2) + area_x;
    if (alignment == 0) {
        *draw_x = uVar2;
        goto LAB_0800bac0;
    }
    if (alignment != 1) {
        if (alignment == 5) {
            *draw_x = uVar2;
        }
        else {
            uVar1 = (320 - area_w) + area_x;
            if (alignment == 3) {
                *draw_x = uVar1;
                goto LAB_0800bab4;
            }
            if (alignment != 6) {
                if (alignment == 7) {
                    *draw_x = area_x;
                }
                else {
                    if (alignment != 8) {
                        if (alignment == 2) {
                            *draw_x = uVar2;
                            goto LAB_0800bab4;
                        }
                        if (alignment != 4) {
                            return;
                        }
                        *draw_x = area_x;
                        goto LAB_0800baba;
                    }
                    *draw_x = uVar1;
                }
            LAB_0800bac0:
                *draw_y = (120 - area_h / 2) + area_y;
                return;
            }
            *draw_x = uVar1;
        }
    LAB_0800baba:
        *draw_y = (240 - area_h) + area_y;
        return;
    }
    *draw_x = area_x;
LAB_0800bab4:
    *draw_y = area_y;
    return;
}


void MainWindow::drawBitmapSimple(uint32_t *imgPtr,int draw_x,int draw_y)

{
    uint uVar1;
    int iVar2;
    int iVar3;
    uint16_t *imgLinePtr;
    uint16_t *imgPixelPtr;


    imgLinePtr = (uint16_t *)(imgPtr[2] - BASE_ADDR + m_fileContent.constData());
    for (iVar3 = 0; iVar3 < (int)(*imgPtr >> 21); iVar3 = iVar3 + 1) {
        uVar1 = draw_x;
        for (iVar2 = 0; iVar2 < (int)((*imgPtr << 11) >> 21); iVar2 = iVar2 + 1) {
            imgPixelPtr = imgLinePtr;
            if ((uVar1 < 320) && (draw_y < 240)) {
                imgPixelPtr = imgLinePtr + 1;
                write_byte_framebuffer_or_display(uVar1,draw_y,*imgLinePtr);
            }
            uVar1 = uVar1 + 1;
            imgLinePtr = imgPixelPtr;
        }
        draw_y = draw_y + 1;
    }
    return;
}


void MainWindow::drawBitmap4bppColorPalette(uint32_t *imgPtr,int draw_x,int draw_y)

{
    uint32_t imgInfos;
    uint8_t *color_rgba;
    int shift;
    uint uVar2;
    int col;
    int line;
    uint8_t* imgDataPtr;
    int iVar4;
    uint8_t alpha;
    imgInfos = *imgPtr;

    uint cols = ((imgInfos >> 10) & 0x7FF);
    uint lines =   ((*imgPtr >> 21) & 0x7FF);

    imgDataPtr = (uint8_t*)(imgPtr[2] - BASE_ADDR + m_fileContent.constData());

    for (line = 0; line < (int) lines ; line = line + 1) {
        iVar4 = draw_x;
        for (col = 0; col < (int)cols ; col = col + 1) {
            if ((col & 1U) == 0) {
                shift = 4;
            }
            else {
                shift = 0;
            }

            uint index = imgDataPtr[64 + col / 2 + line * ((cols + 1) / 2 )] >> shift;
            color_rgba = (uint8_t *)(imgDataPtr + ((index & 0x0F) * 4));
            alpha = color_rgba[3];
            //qDebug() << "RGBA" << color_rgba[0] << color_rgba[1] << color_rgba[2] << color_rgba[3] << "Index " << (index & 0x0F);
            if ((((alpha != 0) && (-1 < draw_x)) && (draw_x < 320)) && (draw_y < 240)) {
                uVar2 = (uint)((int)(short)(ushort)color_rgba[2] * (int)(short)(ushort)alpha) >> 8;
                write_byte_framebuffer_or_display
                    (draw_x,draw_y,
                     ((int)uVar2 >> 3) << 0xb |
                         ((uint)((int)(short)(ushort)color_rgba[1] * (int)(short)(ushort)alpha) >> 10) <<
                             5 | (int)((uint)((int)(short)(ushort)*color_rgba * (int)(short)(ushort)alpha) >>
                                  8) >> 3);
            }
            draw_x = draw_x + 1;
        }
        draw_y = draw_y + 1;
        draw_x = iVar4;
    }
    return;
}


void MainWindow::drawBitmap8bppColorPalette(uint32_t *imgPtr,int draw_x,int draw_y)
{
    uint8_t alpha;
    uint8_t* imgDataPtr;
    uint8_t *pbVar2;
    int iVar4;
    int col;
    int line;

    uint cols = ((*imgPtr >> 10) & 0x7FF);
    uint lines =   ((*imgPtr >> 21) & 0x7FF);


    imgDataPtr = (uint8_t*)(imgPtr[2] - BASE_ADDR + m_fileContent.constData());

    for (line = 0; line < (int)lines; line = line + 1) {
        iVar4 = draw_x;
        for (col = 0; col < (int)cols; col = col + 1) {
            uint index = imgDataPtr[1024 + col + line * cols];
            pbVar2 = imgDataPtr + (index * 4);
            alpha = pbVar2[3];
            if ((((alpha != 0) && (-1 < iVar4)) && (iVar4 < 320)) && (draw_y < 240)) {
                write_byte_framebuffer_or_display
                    (iVar4,draw_y,
                     ((int)((uint)((int)(short)(ushort)pbVar2[2] * (int)(short)(ushort)alpha) >> 8) >>
                      3) << 11 |
                         ((uint)((int)(short)(ushort)pbVar2[1] * (int)(short)(ushort)alpha) >> 10) << 5 |
                         (int)((uint)((int)(short)(ushort)*pbVar2 * (int)(short)(ushort)alpha) >> 8) >> 3)
                    ;
            }
            iVar4 = iVar4 + 1;
        }
        draw_y = draw_y + 1;
    }
    return;
}


void MainWindow::draw_image(uint32_t *imgPtr,int32_t align_mode,int32_t x_pos,int y_pos)

{
  uint8_t imgType;
  uint draw_x;
  uint draw_y;
  uint w = (*imgPtr << 11) >> 21;
  uint h = *imgPtr >> 21;
  imgType = *(uint8_t *)imgPtr & 0x1f;

  ui->wSpinbox->setValue(w);
  ui->hSpinbox->setValue(h);
  ui->modeSpinBox->setValue(imgType);
  ui->datalenSpinBox->setValue(imgPtr[1]);

  compute_draw_coord_for_alignment
            (align_mode,w ,h,x_pos,y_pos,&draw_x,&draw_y);
  if (imgType == 1) {
    drawBitmapSimple(imgPtr,draw_x,draw_y);
  }
  else if (imgType == 3) {
    drawBitmap4bppColorPalette(imgPtr,draw_x,draw_y);
  }
  else if (imgType == 4) {
    drawBitmap8bppColorPalette(imgPtr,draw_x,draw_y);
  }
  else {
    //printf(s_can't_support_img_format_0800cdd4);
  }
  return;
}


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow),
    m_screenImage(320,240,QImage::Format_RGB16)
{
    ui->setupUi(this);

    QFile f("/home/pila/Bureau/2C23T_RE/27604f59f32c4e22801b61c528475962/V2.0.2/F2C23T-EN-V2.0.2.bin");

    if (f.open(QIODevice::ReadOnly)) {
        m_fileContent = f.readAll();
        f.close();
        qDebug() << "Loaded file size" << m_fileContent.length();
    }

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_addrSpinBox_editingFinished()
{
    m_screenImage.fill(0x0000);
    int spinboxval = ui->addrSpinBox->value();
    int addr = spinboxval - BASE_ADDR;
    qDebug() << "Addr" << QString::number(addr,16);
    if (addr > 0 && addr < m_fileContent.size()) {
        uint8_t* ptr = (uint8_t*)m_fileContent.constData() + addr;
        draw_image((uint32_t*)ptr ,0,0,0);
        ui->imgLabel->setPixmap(QPixmap::fromImage(m_screenImage));
    }
}


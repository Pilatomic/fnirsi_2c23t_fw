#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:

    void on_addrSpinBox_editingFinished();

private:
    QImage m_screenImage;
    QByteArray m_fileContent;
    Ui::MainWindow *ui;
    void write_byte_framebuffer_or_display(int x_pos, int y_pos, uint16_t color);
    void compute_draw_coord_for_alignment(int alignment, int area_w, int area_h, int area_x, int area_y, uint *draw_x, uint *draw_y);
    void drawBitmapSimple(uint32_t *imgPtr, int draw_x, int draw_y);
    void drawBitmap4bppColorPalette(uint32_t *imgPtr, int draw_x, int draw_y);
    void drawBitmap8bppColorPalette(uint32_t *imgPtr, int draw_x, int draw_y);
    void draw_image(uint32_t *imgPtr, int32_t align_mode, int32_t x_pos, int y_pos);
};
#endif // MAINWINDOW_H
